import sys

sys.path.append("..")

import robot
import Risa 
import Ondra

from robot.turtlebot import Turtlebot
from rospy import Rate, get_time, sleep, Duration
import time
import cv2
import numpy as np
import math
import matplotlib.pyplot as plt

#if  __name__ == "__main__":
 #   turtle = Turtlebot(rgb=True, depth=True, pc=True)
 #   rate = Rate(10)
  #  t = get_time()

# turn in radians
def turn_rad(radians,turtle, rate, t):
    if radians > 2*math.pi:
        radians = 2*math.pi
    odometry = turtle.get_odometry()
    odometryact = turtle.get_odometry()
    if radians > 0:
        speed = 0.4
    else:
        speed = -0.4
    radians = abs(radians)
    while (abs(abs(odometryact[2] - odometry[2]) - radians) > 0.3):
        print(odometryact[2], odometry[2], radians)
        odometryact = turtle.get_odometry()
        turtle.cmd_velocity(linear=0, angular=speed)
        rate.sleep()

# drive forward in meters
def drive_forward(distance, turtle, rate, t):
    odometry = turtle.get_odometry()
    adistance = 0
    while (abs(distance - adistance) > 0.1):
        print(distance, adistance)
        odometryact = turtle.get_odometry()
        xa = odometryact[0] - odometry[0]
        ya = odometryact[1] - odometry[1]
        adistance = math.sqrt(xa*xa + ya*ya)
        turtle.cmd_velocity(linear=0.2, angular=0)
        rate.sleep()

# drive from here to point in x(forward) y(side) coordinates
def drive_xz(front, side, turtle, rate, t):
    turtle.reset_odometry()
    angle =  math.atan(side/front)
    distance = math.sqrt(front*front + side*side)
    turn_rad(angle, turtle, rate, t)
    drive_forward(distance, turtle, rate, t)

# drive from here to point under a(angle) in d(distance)
def drive_ad(angle, distance, turtle, rate, t):
    turn_rad(angle, turtle, rate, t)
    drive_forward(distance, turtle, rate, t)