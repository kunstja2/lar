import sys 
import time
import cv2
import numpy as np
import math
import matplotlib.pyplot as plt

import Risa
import Ondra
import Jarda

from robot.turtlebot import Turtlebot
from rospy import Rate, get_time, sleep, Duration


from Jarda import basic_movement as bm
from Jarda import image_processing as improc

turtle = Turtlebot(rgb=True, depth=True, pc=True)
rate = Rate(10)
t = get_time()

time.sleep(1)
rbg_image = turtle.get_rgb_image()
pointcloud = turtle.get_point_cloud()

img_blured = improc.blur_img(rbg_image)               #
masked_img = improc.mask_color(img_blured, "yellow")       #
#V_masked_img = mask_color(img, "violet")
result = improc.find_center(masked_img, pointcloud)    
print(result)
x = result[0][1][0]
print(x)
z = result[0][1][2]
print(z)

bm.drive_xz(x, z, turtle, rate, t)


