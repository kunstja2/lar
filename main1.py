import sys 
sys.path.append("c:\python\lib\site-packages")
import time
import cv2
import numpy as np
import math
import matplotlib.pyplot as plt

import Risa
import Ondra
import Jarda

def blur_img(img, blur=150):
    # Bilateral Blur
    blured = cv2.bilateralFilter(img, 10, blur, 35)
    return blured

def depth_of_point(pc, point):  #pc = pointcloud image, point = (x,y,z) of pixel in RGB image.
    dist = pc[point[1]][point[0]]
    return dist


def find_center(myimg, pc_img, prt=False): #------------------------------------------------------------------------
    ### Return result-image with red center points, number of found areas

    # Convert the image to grayscale
    img = cv2.cvtColor(myimg, cv2.COLOR_BGR2GRAY)

    # Threshold the image to separate the white areas
    thresh = cv2.threshold(img, 20, 255, cv2.THRESH_BINARY)[1]

    # Find the contours of the white areas
    contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Compute the screen size
    screen_size = img.shape[0] * img.shape[1]

    # Compute the center point of each white area
    center_points = []
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > 0.05 * screen_size:            # allow only bigger areas
            moments = cv2.moments(contour)
            if moments['m00'] != 0:
                cx = int(moments['m10'] / moments['m00'])
                cy = int(moments['m01'] / moments['m00'])
                center_points.append((cx, cy))

    # Plot the center points on the original image
    result = myimg.copy()
    result_points = []
    for point in center_points:
        cv2.circle(result, point, 5, (0, 0, 255), -1)
        # Compute point distance in PC-img
        dist = depth_of_point(pc_img, point)
        result_points.append((point, dist))       #format as ((240, 480)(-1.001, 0.03, 1.4))

    # How many center-points were found?
    num_of_points = len(center_points)

    if prt == True:
        cv2.imshow("filtered img", result)

    return result_points


def mask_color(img, color="violet"): #---------------------------------------------------------
    # Convert to HSV color space
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    if color == "violet":
        lb = 110
        ub = 150
    elif color == "yellow":
        lb = 20
        ub = 40
    # Define the lower and upper bounds of the violet color in HSV
    lower = np.array([lb, 50, 50])
    upper = np.array([ub, 255, 255])

    # Create mask using the lower and upper bounds
    mask = cv2.inRange(hsv_img, lower, upper)
    

    # Apply the mask on the original image
    result = cv2.bitwise_and(img, img, mask=mask)
        #result = np.zeros_like(img)                # Color the found areas to White
        #result[mask != 0] = (255, 255, 255)
    return result


def line_up_with_left(pos1,x1,pos2,x2):
    if x1 < x2:
        if pos1 < 300:
            return -1
        elif pos1 > 340:
            return 1
        else:
            return 0
    else:
        if pos2 < 300:
            return -1
        elif pos2 > 340:
            return 1
        else:
            return 0

from robot.turtlebot import Turtlebot
from rospy import Rate, get_time, sleep, Duration
from Ondra import pc_img_process
from Jarda import basic_movement 
from Risa import movement_fce

PARKING_DISTANCE = 0.35

turtle = Turtlebot(rgb=True, depth=True, pc=True)
rate = Rate(10)
t = get_time()
ANGLE = np.pi / 3
STATE = 1

time.sleep(0.5)
print("TURTEBOT ready to go")

while True :
    if STATE == 0: # zaparkoval jsem a hraju uloha konci
        print("STATE = 0")
        exit()

    elif STATE == 1: # sjem pred garazi a zajizim
        start_time = time.time()
        print("STATE = 1gttttt")
        img = turtle.get_rgb_image()
        pc_img = turtle.get_point_cloud()
        mask_img = mask_color(img, "yellow")
        points = find_center(mask_img,pc_img,True)
        print(POINTS)
        if points:

            x,y,z = points[0][1]
            pix_x1, _ = points[0][0]
            print(x,y,z)
            distance = np.sqrt(x*x+z*z)
            print("distance = ", distance)
            cv2.waitKey(1)
            
            rotation = line_up_with_left(pix_x1, x, pix_x1, x)
            print("rotation = " + str(rotation))
            turtle.cmd_velocity(linear=0, angular=rotation)

            if distance > PARKING_DISTANCE:
                #turtle.cmd_velocity(linear = 0.0, angular = 0)
                print("going")
            else:
                STATE = 0
        stop_time = time.time()
        print(stop_time-start_time)
        
    elif STATE == 2: # jedu do polohy před garaz
        print("STATE = 2")
        img = turtle.get_rgb_image()        #rgb img
        pc_img = turtle.get_point_cloud()   #pc img
        mask_img = pc_img_process.mask_color(img, "violet")
        points = pc_img_process.find_center(mask_img,pc_img,True)
        print("points",points)

        if len(points)>=2:
            x1,y1,z1 = points[0][1]    # distances in meters
            print(x1,y1,z1)

            x2,y2,z2 = points[1][1]    # distances in meters
            print(x2,y2,z2)



            # dist1 = np.sqrt(x1*x1 + z1*z1)
            # dist = np.sqrt(x2*x2 + z2*z2)

            # avg_dist = (dist1 + dist2)/2
            

            pix_x1, _ = points[0][0]
            pix_x2, _ = points[1][0]

            rotation = movement_fce.line_up_with_left(pix_x1, x1, pix_x2, x2)
            print("rotation = " + str(rotation))
            turtle.cmd_velocity(linear=0, angular=rotation)

            # pix_dist = abs(pix_x1) + abs(pix_x2)

            # anglex = (pix_dist/640) * ANGLE         # uhel o ktery se otocit, abych miril na sloupek

            # if dist1 > dist2
                

            # elif dist1 < dist2:
            #     ...    

        else:
            print("Vidim jen jeden")
            #otacej se a hledej fialovou
    
    elif STATE == 3: # hledam vjezd "fialova barba"
        turtle.play_sound(sound_id=0)
        time.sleep(1)
    rate.sleep()





