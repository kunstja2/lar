import sys
sys.path.append("/home.nfs/kunstja2/lar/robot")
sys.path.append("/home.nfs/rubasric/lar/lar/robot")
from turtlebot import Turtlebot
from rospy import Rate, get_time, sleep, Duration
import time
import cv2
import numpy as np
import math
import matplotlib.pyplot as plt

turtle = Turtlebot(rgb=True, depth=True, pc=True)
imgnum = 1
rate = Rate(10)
t = get_time()
turtle.play_sound(sound_id=0)

# turn in radians
def turn_rad(radians):
    if radians > 2*math.pi:
        radians = 2*math.pi
    odometry = turtle.get_odometry()
    odometryact = turtle.get_odometry()
    while (abs(abs(odometryact[2] - odometry[2]) - radians) > 0.3):
        print(odometryact[2], odometry[2], radians)
        odometryact = turtle.get_odometry()
        turtle.cmd_velocity(linear=0, angular=0.4)
        rate.sleep()

# drive forward in meters
def drive_forward(distance):
    odometry = turtle.get_odometry()
    adistance = 0
    while (abs(distance - adistance) > 0.1):
        print(distance, adistance)
        odometryact = turtle.get_odometry()
        xa = odometryact[0] - odometry[0]
        ya = odometryact[1] - odometry[1]
        adistance = math.sqrt(xa*xa + ya*ya)
        turtle.cmd_velocity(linear=0.2, angular=0)
        rate.sleep()

# drive from here to point in x(forward) y(side) coordinates
def drive_xy(front, side):
    turtle.reset_odometry()
    angle =  math.atan(side/front)
    distance = math.sqrt(front*front + side*side)
    turn_rad(angle)
    drive_forward(distance)

# drive from here to point under a(angle) in d(distance)
def drive_ad(angle, distance):
    turn_rad(angle)
    drive_forward(distance)

# opens npy file and normalizes it
def load_depth_image(image_name):
    depth_array = np.load(image_name)
    # Preprocess the depth image array
    depth_array = cv2.medianBlur(depth_array, 3) # Median filtering to remove noise
    depth_array = cv2.normalize(depth_array, None, 0, 255, cv2.NORM_MINMAX) # Normalize depth values
    return depth_array

# saves depth image as npy file
def save_npy(nevim):
    global imgnum
    imgnum += 1
    depth_img = turtle.get_depth_image()
    np.save('array' + str(imgnum) + '.npy', depth_img)
    print("OBRAZEK ULOZEN, BUTTON PRESSED.")
    turtle.play_sound(sound_id=3)

# colred image of distance
def visualize_depth(file_name):
    depth_array = np.load("array5.npy")
    # Set the colormap to use for the conversion
    cmap = plt.cm.jet  # Or any other colormap you prefer
    # Normalize the depth values to the range [0, 1]
    depth_normalized = (depth_array - np.min(depth_array)) / (np.max(depth_array) - np.min(depth_array))
    # Apply the colormap to the normalized depth array
    depth_colored = cmap(depth_normalized)
    # Display the color image
    plt.imshow(depth_colored)
    plt.show()


turtle.register_button_event_cb(save_npy)

turtle.play_sound(sound_id=0)
while get_time() - t < 1:
    rate.sleep()


drive_forward(2)
#turn_rad(1)
#drive_xy(2, 2)

