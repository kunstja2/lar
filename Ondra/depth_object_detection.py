import numpy as np
import cv2

depth_array = np.load('array5.npy')

# Convert the depth array to an 8-bit grayscale image
depth_image = cv2.convertScaleAbs(depth_array, alpha=0.03)
# Threshold the grayscale image to create a binary mask
_, binary_mask = cv2.threshold(depth_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
# Apply morphological operations to remove noise and fill gaps in the binary mask
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))

binary_mask = cv2.erode(binary_mask, kernel, iterations=10) # change number of iterrations to in/decrease sensitivity
binary_mask = cv2.dilate(binary_mask, kernel, iterations=2)
# Find contours in the binary mask
contours, _ = cv2.findContours(binary_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# Draw bounding boxes around the detected objects
c = 0
for contour in contours:
    c+=1
    x, y, w, h = cv2.boundingRect(contour) # from left, from top, width, height
    print(x, y, w, h)
    cv2.rectangle(depth_image, (x, y), (x+w, y+h), (255, 255, 255), c) # highlight
    roi = depth_array[y:y+h, x:x+w] # cut out the object area
    average_value = np.mean(roi) # get average distance of the object
    print(average_value)


# Display the depth image with bounding boxes
cv2.imshow('Object Detection', depth_image)
cv2.waitKey(0)
cv2.destroyAllWindows()

