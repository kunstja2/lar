import sys
sys.path.append('/usr/local/lib/python3.9/site-packages')
import time
import cv2
import numpy as np

def blur_img(img, blur=150):
    # Bilateral Blur
    blured = cv2.bilateralFilter(img, 10, blur, 35)
    return blured

def depth_of_point(pc, point):  #pc = pointcloud image, point = (x,y,z) of pixel in RGB image.
    dist = pc[point[1]][point[0]]
    return dist


def find_center(myimg, pc_img, prt=False): #------------------------------------------------------------------------
    ### Return result-image with red center points, number of found areas

    # Convert the image to grayscale
    img = cv2.cvtColor(myimg, cv2.COLOR_BGR2GRAY)

    # Threshold the image to separate the white areas
    thresh = cv2.threshold(img, 20, 255, cv2.THRESH_BINARY)[1]

    # Find the contours of the white areas
    contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Compute the screen size
    screen_size = img.shape[0] * img.shape[1]

    # Compute the center point of each white area
    center_points = []
    for contour in contours:
        area = cv2.contourArea(contour)
        if area > 0.05 * screen_size:            # allow only bigger areas
            moments = cv2.moments(contour)
            if moments['m00'] != 0:
                cx = int(moments['m10'] / moments['m00'])
                cy = int(moments['m01'] / moments['m00'])
                center_points.append((cx, cy))

    # Plot the center points on the original image
    result = myimg.copy()
    result_points = []
    for point in center_points:
        cv2.circle(result, point, 5, (0, 0, 255), -1)
        # Compute point distance in PC-img
        dist = depth_of_point(pc_img, point)
        result_points.append((point, dist))       #format as ((240, 480)(-1.001, 0.03, 1.4))

    # How many center-points were found?
    num_of_points = len(center_points)

    if prt == True:
        cv2.imshow("filtered img", result)

    return result_points


def mask_color(img, color="violet"): #---------------------------------------------------------
    # Convert to HSV color space
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    if color == "violet":
        lb = 110
        ub = 150
    elif color == "yellow":
        lb = 20
        ub = 40
    # Define the lower and upper bounds of the violet color in HSV
    lower = np.array([lb, 50, 50])
    upper = np.array([ub, 255, 255])

    # Create mask using the lower and upper bounds
    mask = cv2.inRange(hsv_img, lower, upper)
    

    # Apply the mask on the original image
    result = cv2.bitwise_and(img, img, mask=mask)
        #result = np.zeros_like(img)                # Color the found areas to White
        #result[mask != 0] = (255, 255, 255)
    return result

#--------------------------------------------------------------------------------------------------





# HOW TO USE ---------
# i've got rgb_img and pc_img
# blured_img = blur_img(rgb_img)       # Optional
# masked_img = mask_color(blured_img, ')
# result_list = find_center(masked_img)



